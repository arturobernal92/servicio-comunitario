# Servicio Comunitario #

Bueno, he aquí nuestro repositorio para nuestro Servicio Comunitario.

### Por qué elegí bitbucket y no git? ###

Porque bitbucket me permite crear repositorios privados sin la necesidad de tener que comprar alguna suscripción. Aunque este no es privado por problemas con nuestro servidor web gratuito

### Comandos básicos ###

* Clonar este repositorio: ````git clone https://ArturoB@bitbucket.org/ArturoB/servicio-comunitario.git````
* Subir nuevos cambios:
* Verifican su estatus: ````git status````
* Añaden cambios localmente: ````git add````
* Realizar commit: ````git commit -m "commit de prueba"````
* Subir cambios a este repositorio: ````git push origin master````

Dudas para utilizar git en su pc?

Si ese es el caso, me pueden preguntar directamente como crear repositorios en la carpeta deseada y ese tipo de cosas.


Nuestro tablero de trello es: ````https://trello.com/b/QjWG1n24/servicio-comunitario#```` Por favor revisar el tablero continuamente para mantener un orden en las actividades